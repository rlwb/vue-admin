import axios from 'axios'
import qs from 'qs'
import { Message } from 'element-ui'
import store from '@/store'
import router from '@/router'
import $ from 'jquery'
//开发环境的URL
// const baseURL = 'http://192.168.1.170:8888/api/private/v1/'
//测试环境
// const baseURL = 'http://test.abc.com/api/private/v1/'
//线上环境
// const baseURL = 'http://www.abc.com/api/private/v1/'

let baseURL;
if(process.env.NODE_ENV === 'development') {
    baseURL = 'http://192.168.1.170:8888/api/private/v1/'
}else if(process.env.NODE_ENV === 'production') {
    baseURL = 'http://www.abc.com/api/private/v1/'
}else {
    baseURL = 'http://test.abc.com/api/private/v1/'
}

axios.defaults.baseURL = baseURL
// axios.defaults.baseURL = 'http://localhost:8888/api/private/v1/'
// export default function http(url, data, method, params){
    // return new Promise((resolve, reject) => {
    //     axios({
    //         url,
    //         data,
    //         method,
    //         params

    //     })
    //     .then(res => {
    //         if (res.data.meta.status >= 200 && res.data.meta.status < 300) {
    //             resolve(res.data)
    //         }else {
    //             Message.error(res.data.meta.msg)
    //             reject(res.data.meta)
    //         }
    //     }).catch(err => {
    //         Message.error(err)
    //         reject(err)
    //     })
    // })
//     return axios({
//         url,
//         data,
//         method,
//         params
//     })
//     .then(res => {
//         if (res.data.meta.status >= 200 && res.data.meta.status < 300) {
//             return Promise.resolve(res.data)
//         }else {
//             Message.error(res.data.meta.msg)
//             return Promise.reject(res.data.meta)
//         }
//     }, (err) => {
//         Message.error(err)
//             return Promise.reject(err)
//     })

// }

axios.interceptors.request.use(function (config) {
    // 在发送请求之前做些什么
    if(store.state.token) {
        config.headers.Authorization = store.state.token
    }
    return config;
}, function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
});

// 添加响应拦截器
axios.interceptors.response.use(function (response) {
    // 对响应数据做点什么
    if(response.data.meta.msg==='无效token') {
        router.replace({
            name: 'login'
        })
    } else if (response.data.meta.msg === '用户无权限进入这个页面') {
        router.replace({
            name: '404'
        })
    }
    return response;
}, function (error) {
    // 对响应错误做点什么
    return Promise.reject(error);
});


export const http = async function(url, data, method, params) {
    try {
        let res = await axios({
            url,
            data,
            method: method ? method.toUpperCase() : 'GET',
            params
        })
        if (res.data.meta.status >= 200 && res.data.meta.status < 300) {
            return Promise.resolve(res.data)
        }else {
            Message.error(res.data.meta.msg)
            return Promise.reject(res.data.meta)
        }

    }catch(err) {
        Message.error(err)
        return Promise.reject(err)
    }
}

// export const http = function(url, data, method, params) {
//     return new Promise((resolve, reject)=>{
//         var xmlhttp = new XMLHttpRequest();
//         var str = ''
//         xmlhttp.onreadystatechange = function () {
//             if (this.readyState == 4 && this.status == 200) {
//                 console.log(this.response)
//                 return resolve(JSON.parse(this.response))
//             }
//         };
//         if(params){
//             let arr = Object.keys(params);
//             for(let i = 0, j = arr.length; i < j; i ++) {
//                 str += `${arr[i]}=${params[arr[i]]}&`
//             }
//             str = '?'+str
//         }
//         xmlhttp.open(method, `${baseURL}${url}${str}` , true);
//         xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
//         xmlhttp.send(JSON.stringify(data));
//     })
// }

// export const http = async function (url, data, method, params) {
//     let str = ''
//     if(params) {
//         str = qs.stringify(params)
//     }
//     let res = await fetch(baseURL + url, {
//         body: JSON.stringify(data) + str, 
//         headers: {
//             'user-agent': 'Mozilla/4.0 MDN Example',
//             'content-type': 'application/json'
//         },
//         method
//     })
//     return res.json().then(res => {
//         console.log(res)
//         return Promise.resolve(res)
//     })
// }

// export const http = function (url, data, method, params) {
//     let str = ''
//     if(params) {
//         str = qs.stringify(params)
//     }
//     return $.ajax({
//         url: baseURL + url + '?' + str,
//         headers: {
//             Authorization: store.state.token || ''
//         },
//         data,
//         method
//     })
//     .done(res => {
//         console.log(res)
//         return Promise.resolve(res)
//     })
// }


export const _baseUrl = baseURL